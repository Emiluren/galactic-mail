use luminance::blending::{Blending, Equation, Factor};
use luminance::context::GraphicsContext;
use luminance::depth_test::DepthComparison;
use luminance::pipeline::{PipelineState, TextureBinding};
use luminance::pixel::{NormRGBA8UI, NormUnsigned};
use luminance::render_state::RenderState;
use luminance::shader::Uniform;
use luminance::tess::Mode;
use luminance::texture::{Dim2, GenMipmaps, Sampler, Texture};
use luminance::UniformInterface;
use luminance_gl::gl33::GL33;
use luminance_sdl2::GL33Surface;
use glsp::prelude::*;
use sdl2::event::Event;

use std::process::exit;
use std::time::Instant;

const VS: &'static str = include_str!("sprite.vert");
const FS: &'static str = include_str!("sprite.frag");

const WINDOW_WIDTH: u32 = 640;
const WINDOW_HEIGHT: u32 = 480;

fn main() {
    let surface = GL33Surface::build_with(
        |video| video.window("Galactic Mail", WINDOW_WIDTH, WINDOW_HEIGHT)
    );

    match surface {
        Ok(surface) => {
            main_loop(surface);
        }

        Err(e) => {
            eprintln!("Could not create graphics surface:\n{}", e);
            exit(1);
        }
    }
}

#[derive(UniformInterface)]
struct ShaderInterface {
    tex: Uniform<TextureBinding<Dim2, NormUnsigned>>,
    scale: Uniform<[f32; 2]>,
}

struct Sprite {
    tex: Texture<GL33, Dim2, NormRGBA8UI>,
    size: (u32, u32),
}

fn main_loop(mut surface: GL33Surface) {
    let start_t = Instant::now();
    let back_buffer = surface.back_buffer().unwrap();

    let mut asteroid_sprite = load_sprite(&mut surface, "assets/Asteroid.gif");
    let mut background_sprite = load_sprite(&mut surface, "assets/Background.bmp");
    let mut moon_sprite = load_sprite(&mut surface, "assets/Moon.gif");

    let built_program = surface
        .new_shader_program::<(), (), ShaderInterface>()
        .from_strings(VS, None, None, FS)
        .expect("Could not create program");

    for warn in &built_program.warnings {
        println!("{}", warn);
    }

    let mut program = built_program.ignore_warnings();

    let tess = surface
        .new_tess()
        .set_vertex_nb(4)
        .set_mode(Mode::TriangleFan)
        .build()
        .unwrap();

    let render_st = &RenderState::default()
        .set_blending(Blending {
            equation: Equation::Additive,
            src: Factor::SrcAlpha,
            dst: Factor::SrcAlphaComplement,
        })
        .set_depth_test(DepthComparison::Always);

    let runtime = Runtime::new();

    runtime.run(|| {
        glsp::load("script/main.lisp")?;
        Ok(())
    });

    let mut event_pump = surface.sdl().event_pump().unwrap();
    'app: loop {
        // handle events
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} => {
                    break 'app;
                }
                _ => {}
            }
        }

        let render = surface.new_pipeline_gate().pipeline(
            &back_buffer,
            &PipelineState::default().set_clear_color([0.2, 0.2, 0.2, 1.]),
            |pipeline, mut shd_gate| {
                for sprite in [&mut background_sprite, &mut moon_sprite].iter_mut() {
                    let bound_tex = pipeline.bind_texture(&mut sprite.tex)?;
                    let (width, height) = sprite.size;

                    shd_gate.shade(&mut program, |mut iface, uni, mut rdr_gate| {
                        iface.set(&uni.tex, bound_tex.binding());
                        iface.set(&uni.scale, [
                            width as f32 / WINDOW_WIDTH as f32,
                            height as f32 / WINDOW_HEIGHT as f32,
                        ]);

                        rdr_gate.render(render_st, |mut tess_gate| {
                            tess_gate.render(&tess)
                        })
                    })?;
                }
                Ok(())
            },
        ).assume();

        if render.is_ok() {
            surface.window().gl_swap_window();
        } else {
            break 'app;
        }
    }
}

fn load_sprite(context: &mut GL33Surface, path: &str) -> Sprite {
    let img = image::open(path).map(|img| img.flipv().to_rgba8())
        .expect(&format!("Could not load {}", path));

    let (width, height) = img.dimensions();
    let texels = img.into_raw();

    let mut tex =
        Texture::new(context, [width, height], 0, Sampler::default())
        .expect("Failed to create texture");

    tex.upload_raw(GenMipmaps::No, &texels).unwrap();

    Sprite {
        tex,
        size: (width, height),
    }
}
